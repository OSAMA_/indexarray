package com.epam.de.dsa;

import java.util.Arrays;

public class IndexArray {

    public int[] indexArray(int[] elements){
        int n = elements.length;
        int[] resultArray = new int[n];
        Arrays.fill(resultArray, -1);
        for (int i = 0; i < n; i++) {
            if (elements[i] != -1) {
                resultArray[elements[i]] = elements[i];
            }
        }
        return resultArray;
    }
}
