package com.epam.de.dsa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IndexArrayTest {

    IndexArray indexArray;

    @BeforeEach
    void setUp(){
        indexArray = new IndexArray();
    }

    @Test
    void indexArrayTest(){
        int[] arr = {8, -1, 6, 1, 9, 3, 2, 7, 4, -1};
        int[] expected_arr = {-1, 1, 2, 3, 4, -1, 6, 7, 8, 9};
        assertArrayEquals(expected_arr,indexArray.indexArray(arr));
    }
}